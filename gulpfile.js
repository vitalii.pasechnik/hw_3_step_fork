import gulp from 'gulp';
import rename from 'gulp-rename';
import { deleteAsync } from 'del';
import autoPrefixer from 'gulp-autoprefixer';
import imagemin from 'gulp-imagemin';
import browserSync from 'browser-sync';
import concat from 'gulp-concat';
import GulpUglify from 'gulp-uglify';

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

import pkg from 'gulp';
const { parallel, series } = pkg;

const project_folder = './dist/';

const path = {
    src: {
        scss: ['./src/scss/**/*.scss', '!./src/scss/**/_*.scss'],
        js: './src/js/**/*.js',
        img: ['./src/img/**/*.*', '!./src/img/**/*.svg'],
        svg: './src/img/**/*.svg',
        fonts: './src/fonts/**/*.*',
    },
    dist: {
        css: `${project_folder}*.css`,
        js: `${project_folder}*.js`,
        img: [`${project_folder}img/**/*.*`, `!${project_folder}img/**/*.svg`],
        svg: `${project_folder}img/**/*.svg`,
        fonts: `${project_folder}fonts/**/*.*`,
    }
}

function buildCSS() {
    deleteAsync(path.dist.css);
    return gulp.src(path.src.scss)
        .pipe(concat('styles.scss'))
        .pipe(sass({
            errorLogToConsole: true,
            outputStyle: "compressed"
        }))
        .pipe(autoPrefixer())
        .pipe(rename({
            suffix: '.min',
            basename: 'styles'
        }))
        .pipe(gulp.dest(project_folder))
        .pipe(browserSync.stream())
}

function buildJS() {
    deleteAsync(path.dist.js);
    return gulp.src(path.src.js)
        .pipe(concat('scripts.js'))
        .pipe(rename({
            suffix: '.min',
            basename: 'scripts'
        }))
        .pipe(GulpUglify())
        .pipe(gulp.dest(project_folder))
        .pipe(browserSync.stream())
}

function buildIMG() {
    deleteAsync(path.dist.img);
    return gulp.src(path.src.img)
        .pipe(gulp.dest('./dist/img'))
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img'))
        .pipe(browserSync.stream())
}

function buildSVG() {
    deleteAsync(path.dist.svg);
    return gulp.src(path.src.svg)
        // .pipe(svgmin())
        .pipe(gulp.dest('./dist/img'))
        .pipe(browserSync.stream())
}
function buildFonts() {
    deleteAsync(path.dist.fonts);
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest('./dist/fonts'))
        .pipe(browserSync.stream())
}

function watcher() {
    gulp.watch('./src/scss/**/*.scss', buildCSS).on('change', browserSync.reload);
    gulp.watch(path.src.js, buildJS).on('change', browserSync.reload);
    gulp.watch(path.src.svg, buildSVG).on('change', browserSync.reload);
    gulp.watch(path.src.img, buildIMG).on('change', browserSync.reload);
    gulp.watch(path.src.fonts, buildFonts).on('change', browserSync.reload);
    gulp.watch('./*.html').on('change', browserSync.reload);
}

function server() {
    browserSync.init({
        server: {
            baseDir: './'
        },
        notify: false,
        port: 3000,
    });
}

const build = () => {
    return gulp.parallel(buildCSS, buildJS, buildIMG, buildSVG, buildFonts);
}

const dev = () => {
    return gulp.parallel(watcher, server);
}

export { dev }
export { build }

gulp.task('default', series(build(), dev()));