// export const BURGER = () => {

const burgerBtn = document.querySelector('.burger__btn');
const menu = document.querySelector('.header__nav-wrapper');

burgerBtn.addEventListener('click', toggleMenu);
document.body.addEventListener('click', isOutsideClick);
document.body.addEventListener('keydown', isEscPress);
window.addEventListener('resize', closeMenuOnDesktop);

function toggleMenu() {
    burgerBtn.classList.toggle('_active');
    menu.classList.toggle('_burger');
}

function closeMenu() {
    burgerBtn.classList.remove('_active');
    menu.classList.remove('_burger');
}

function isOutsideClick(e) {
    const isMenuActive = menu.classList.contains('_burger');
    const clickOnBurger = e.target.closest('.burger__btn');
    const clickOnMenu = e.target.closest('.header__nav');
    isMenuActive && !clickOnBurger && !clickOnMenu && closeMenu();
}

function isEscPress(e) {
    e.keyCode === 27 && closeMenu();
}

function closeMenuOnDesktop() {
    window.innerWidth > 768 && closeMenu();
}
// }